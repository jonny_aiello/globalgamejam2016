﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
 
public class FlashingText : MonoBehaviour {
  
 Text flashingText;
 public string textToFlash = "Timer";
 public string blankText = "";
 public string staticText = "I'M FLASHING NO MORE";

 public float watingSeconds;

 //flag to determine if you want the blinking to happen
 bool isBlinking = true;
  
 void Start(){
  //get the Text component
  flashingText = GetComponent<Text>();
  //Call coroutine BlinkText on Start
  StartCoroutine(BlinkText());
  //call function to check if it is time to stop the flashing.
  StartCoroutine(StopBlinking());
 }
  
 //function to <span id="IL_AD2" class="IL_AD">blink</span> the text 
 public IEnumerator BlinkText(){
  //blink it forever. You can set a terminating condition depending upon your requirement. Here you can just set the isBlinking flag to false whenever you want the blinking to be stopped.
  while(isBlinking){
   //set the Text's text to blank
   flashingText.text = blankText;
   //display blank text for 0.5 seconds
   yield return new WaitForSeconds(watingSeconds);
   //display “I AM FLASHING TEXT” for the next 0.5 seconds
   flashingText.text = textToFlash;
   yield return new WaitForSeconds(watingSeconds); 
  }
 }
 //your logic here. I have set the isBlinking flag to false after 5 seconds
 IEnumerator StopBlinking(){
  //wait for 5 seconds
  yield return new WaitForSeconds(5f);
  //stop the blinking
  isBlinking = false;
  //set a different text just for sake of clarity
  flashingText.text = staticText;
 }
}