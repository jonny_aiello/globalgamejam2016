﻿using UnityEngine;
using System.Collections;

public class movemove : MonoBehaviour {
	public float firstPress = 0;
	// Use this for initialization
	public float smooth ;
	public float tiltAngle = 360.0F;

	public float tiltAroundZ; 

	public int count = 0;

	public InputMinigame im; 
	public GameObject win; 


	
	public AudioClip grind;
	public bool playsound = false;

	void Start(){


	}

	void Update() {
	
//		Debug.Log (move);

		//tiltAroundZ = Mathf.Abs(Input.GetAxis("Horizontal")) * tiltAngle;
		//float tiltAroundX = Input.GetAxis("Vertical") * tiltAngle;
		//float tiltAroundZ = move * tiltAngle;
		tiltAroundZ = (Input.GetAxis("Horizontal")) * tiltAngle;
		Quaternion target = Quaternion.Euler(0, 0, tiltAroundZ);

	

		transform.rotation = Quaternion.Lerp(transform.rotation, target, Time.deltaTime * smooth);



		if (tiltAngle > 360f) {

			tiltAngle =60f;
		}
	
		if (Input.GetKeyDown (KeyCode.LeftArrow) ||Input.GetKeyDown(KeyCode.RightArrow)) {

		

			if (playsound == false) {
				playsound = true; 
				InvokeRepeating ("PlaySound", 0.2f, 0.8f);
				
			}


			var currentTime=Time.time;
			{

				if( count == 0)
				{
					 
					firstPress = Time.time;

		}

				count ++ ;

					if ( count > 20)
				{
					if (currentTime-firstPress<30) 
					{
						Debug.Log("YOU WIN" + null);
						win.SetActive(true); 
						StartCoroutine("TheEnd");
						// im.done = true;
						// count =0;
					}




				}
	}

		}

		if (Input.GetKeyUp (KeyCode.LeftArrow) || Input.GetKeyUp (KeyCode.RightArrow)) {
			CancelInvoke("PlaySound");
			playsound = false;
		}
	}

	public IEnumerator TheEnd(){
	  
	   yield return new WaitForSeconds(1);
	   
	   Application.LoadLevel(0); 
  }
	void PlaySound () {
		GetComponent<AudioSource>().PlayOneShot(grind, 5f);

	}
}
