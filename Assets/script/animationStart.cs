﻿using UnityEngine;
using System.Collections;

public class animationStart : MonoBehaviour {
	public Animation anim;

	public string coffeebean;
	// Use this for initialization
	void Start () {
		anim = GetComponent<Animation>();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.LeftArrow) || Input.GetKeyDown (KeyCode.RightArrow)) {
			if (!anim.IsPlaying(coffeebean))
				anim.Play(coffeebean);
		}
	}
}
