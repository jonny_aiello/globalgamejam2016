﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI; 

public class Printer : MonoBehaviour {

	// Variables
	public Text textUI; 
	public Text answer1b; 
	public Text answer2b;
	public Button button1;
	public Button button2;
	public InputHandler ih;  
	private List<string> printQueue = new List<string>(); 
	public StateMachine sm;
	public TextContainer tc;
	private TextHandler th; 

	private bool response = false; 


	// [[ ----- START ----- ]]
	void Start () {
		// textUI = GetComponent<Text>();
		//tc = th.textBlockList[(int)sm.startState];
		DeactivateButtons(); 
		tc = new TextContainer();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	// [[ ----- LOAD QUEUE ----- ]]
	public void LoadQueue( TextContainer _tc, int _response ){
		// Debug.Log("Text Loaded to printer, Response: " + _response);
 
		printQueue.Clear(); Debug.Log("1a - PrintQueue Cleared" + null);
		if( _response == 0 ){ 
			tc = _tc; 
			foreach( string s in tc.narrativeL ){
				printQueue.Add(s);
				Debug.Log("1b - string loaded: " + printQueue.Count + s);
				} 
		}else if (_response == 1){
			response = true;
			foreach( string s in tc.r1 ){
				printQueue.Add(s);
				Debug.Log("1b - response1 string loaded: " + printQueue.Count + s); 
			}
		}else if (_response == 2){
			response = true; 
			foreach( string s in tc.r2 ){
				printQueue.Add(s);
				Debug.Log("1b - response2 string loaded: " + printQueue.Count + s); 
			}
		}
		if( printQueue[0] != null ){ Print(printQueue[0]);	}
	}

	// [[ ----- LOAD RESPONSE ----- ]]
	// public void LoadResponse( List<string> _responseL ){
	// 	printQueue.Clear();
	// 	foreach( string s in _responseL ){
	// 		printQueue.Add(s);
	// 		Debug.Log("TESTING - string loaded: " + printQueue.Count + s); 
	// 	}
	// 	Print(printQueue[0]);
	// 	response = true; Debug.Log("Response set True" + null);
	// }

	// [[ ----- TURN PAGE ----- ]]
	public void TurnPage(){
		// Debug.Log("Turn Page - tc.a1: " + tc.a1);
		Debug.Log("4 - turn page called" + null);
		if( printQueue.Count > 1 ){ 
			printQueue.RemoveAt(0); 
			Print(printQueue[0]); 
			Debug.Log("4a - page removed, next page printed" + null);
		}
		else if( (tc.a1 != "none") && !response ){
			// poulate buttons
			answer1b.text = tc.a1;
			answer2b.text = tc.a2; 
			ih.narrativeMode = false; //Debug.Log("Printer deactivates narrative" + null);
			ActivateButtons();
			response = false; //Debug.Log("Response set to false" + null);
			Debug.Log("4a - buttons updated, response set false" + null);
			// sm.LoadNextCh();
		} 
		else{
			sm.LoadNextCh(); 
			Debug.Log("4a - no text left, load next chapt" + null);
		}
	}

	// [[ ----- PRINT ----- ]]
	public void Print( string _text ){
		textUI.text = _text; Debug.Log("2 - printing" + null);
	}

	// [[ ----- CHECK FOR END ----- ]]
	public void	CheckForEnd( TextContainer _tc ){
		
	}

	// [[ ----- ACTIVATE BUTTONS ----- ]]
	public void ActivateButtons(){
		button1.gameObject.SetActive(true); 
		button2.gameObject.SetActive(true); 
	}

	// [[ ----- DEACTIVATE BUTTONS ----- ]]
	public void DeactivateButtons(){
		button1.gameObject.SetActive(false); 
		button2.gameObject.SetActive(false); 
	}
}
