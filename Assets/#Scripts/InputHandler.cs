﻿using UnityEngine;
using System.Collections;

public class InputHandler : MonoBehaviour {

	public bool narrativeMode = true; 
	public Printer p;
	private float unlockTime; 

	// Use this for initialization
	void Start () {
		// p = GetComponent<Printer>(); 
	}
	
	// Update is called once per frame
	void Update () {
		if( (narrativeMode && Input.GetButtonDown("Fire1")) && (Time.time > unlockTime) ){
			Debug.Log("3 - input handled" + null);
			p.TurnPage(); 
			unlockTime = Time.time + 0.25f; 
		} 
	
	}
}
