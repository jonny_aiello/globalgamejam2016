﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; 

public class TextContainer{

	public List<string> narrativeL = new List<string>();
	public string a1 = "none"; 
	public string a2 = "none"; 
	public List<string> r1 = new List<string>();
	public List<string> r2 = new List<string>(); 

	// [[ ----- ADD NARRATIVE ----- ]]
	public void AddNarrative( string _s ){
		narrativeL.Add(_s);
	}

	// [[ ----- ADD ANSWER1 ----- ]]
	public void AddAnswer1( string _s ){
		a1 = _s;
	}

	// [[ ----- ADD ANSWER2 ----- ]]
	public void AddAnswer2( string _s ){
		a2 = _s;
	}

	// [[ ----- ADD RESULT1 ----- ]]
	public void AddResult1( string _s ){
		r1.Add(_s);
	}

	// [[ ----- ADD RESULT2 ----- ]]
	public void AddResult2( string _s ){
		r2.Add(_s);
	}

}

public class TextHandler : MonoBehaviour {
	
	// enum
	// private enum TB{ //textblock
	// 	Intro,
	// 	TOTAL
	// }

	// variables
	public List<TextContainer> textBlockList = new List<TextContainer>(); 

	// [[ ----- Awake ----- ]]
	void Awake () {
		for( int i = 0; i < (int)GC.State.TOTAL; i++ ){
			textBlockList.Add( new TextContainer() );
		}

		Debug.Log("TextBlocks Added: " + textBlockList.Count);


		// THIS IS WHERE WE ADD THE GAME TEXT
		// Max Nar characters: 256

		// Intro1
		textBlockList[(int)GC.State.INTRO1].AddNarrative(
			"On a day, like any other day... "
			);
		

		// Intro
		textBlockList[(int)GC.State.INTRO2].AddNarrative(
			"CUSTOMER: Good morning!"
			);
		textBlockList[(int)GC.State.INTRO2].AddNarrative(
			"BARISTA: Happy Hump Day, Mr. Wickhauser! The usual for you?"
			);
		textBlockList[(int)GC.State.INTRO2].AddNarrative(
			"CUSTOMER: That's right, I'd like one cup of Blonde Java with extra cream."
			);
		textBlockList[(int)GC.State.INTRO2].AddNarrative(
			"BARISTA: That'll be $15."
			);
		Debug.Log("Intro1 TextBlocks: " + (int)GC.State.INTRO2);
		
		// SITUATION1
		textBlockList[(int)GC.State.SITUATION1].AddNarrative(
			"BEAN, MUFFLED: Help! Please! Save me!"
			);
		textBlockList[(int)GC.State.SITUATION1].AddNarrative(
			"BARISTA: I'm sorry, but you know the drill."
			);
		textBlockList[(int)GC.State.SITUATION1].AddNarrative(
			"BEAN: Come on, won't you let a poor little coffee bean go?"
			);
		textBlockList[(int)GC.State.SITUATION1].AddAnswer1(
			"Sympathize"
			);
		textBlockList[(int)GC.State.SITUATION1].AddAnswer2(
			"Begin Ritual"
			);
		textBlockList[(int)GC.State.SITUATION1].AddResult1(
			"Barista: Think of it like this: You're sending all your good vibes and energy to people who need them! You'll be a big hero!"
			);
		textBlockList[(int)GC.State.SITUATION1].AddResult1(
			"Bean: A hero, huh? Wowee! But you'd be the bigger hero if you set me free…"
			);
		textBlockList[(int)GC.State.SITUATION1].AddResult2(
			"Barista: C'mon, can't you just give me a little bit of yourself?"
			);
		textBlockList[(int)GC.State.SITUATION1].AddResult2(
			"Bean: Ew, no! You're a creep!"
			);

		Debug.Log("Situation1 TextBlocks: " + (int)GC.State.SITUATION1);

		// SITUATION2
		textBlockList[(int)GC.State.SITUATION2].AddNarrative(
			"BEAN: If you're gonna kill me, can't ya at least ask me my name?!"
			);
		textBlockList[(int)GC.State.SITUATION2].AddNarrative(
			"My best pals, Billie Beans and Java Joe, were made into a puddle of sludge, and you never even knew their names."
			);
		// Answer Text
		textBlockList[(int)GC.State.SITUATION2].AddAnswer1(
			"Sympathize"
			);
		textBlockList[(int)GC.State.SITUATION2].AddAnswer2(
			"Continue Ritual"
			);
		// Results Text 1
		textBlockList[(int)GC.State.SITUATION2].AddResult1(
			"Barista: Sure, tell me your story! What's your name? Where did you grow up? Who was your favorite teacher in school?"
			);
		textBlockList[(int)GC.State.SITUATION2].AddResult1(
			"Bean Response: Why should I tell you all of that? Are you some kind of bean historian?"
			);
		// Restluts Text 2
		textBlockList[(int)GC.State.SITUATION2].AddResult2(
			"It'd be easier for both of us if I didn't."
			);

		// SITUATION3
		textBlockList[(int)GC.State.SITUATION3].AddNarrative(
			"BEAN: Uhh, can you please tell my family I love them? Give my son a kiss goodbye for me?"
			);
		// Answer Text
		textBlockList[(int)GC.State.SITUATION3].AddAnswer1(
			"Sympathize"
			);
		textBlockList[(int)GC.State.SITUATION3].AddAnswer2(
			"Continue Ritual"
			);
		// Results Text 1
		textBlockList[(int)GC.State.SITUATION3].AddResult1(
			"Barista: Of course. I'll give him a kiss AND an extra big pinch on the cheek. "
			);
		textBlockList[(int)GC.State.SITUATION3].AddResult1(
			"Bean: Are you saying you wanna kill him too?!"
			);
		// Restluts Text 2
		textBlockList[(int)GC.State.SITUATION3].AddResult2(
			"Barista: I will. Just tell me where to find him and I'll tell him Daddy went on a big trip.2"
			);
		textBlockList[(int)GC.State.SITUATION3].AddResult2(
			"Bean: Gee, that means a lot! Thanks!"
			);

		// SITUAION4
		textBlockList[(int)GC.State.SITUATION4].AddNarrative(
			"BEAN: Look. You know what's going on here as well as I do. We're both servants of the capitalism machine; you're the maker, I'm the product."
			);
		textBlockList[(int)GC.State.SITUATION4].AddNarrative(
			"Both of us would have a freer conscience if we stopped being cogs."
			);
		textBlockList[(int)GC.State.SITUATION4].AddNarrative(
			"We could make a real difference! We could be revolutionaries! Won't you listen to reason?"
			);
		// Answer Text
		textBlockList[(int)GC.State.SITUATION4].AddAnswer1(
			"Sympathize"
			);
		textBlockList[(int)GC.State.SITUATION4].AddAnswer2(
			"Complete Ritual"
			);
		// Results Text 1
		textBlockList[(int)GC.State.SITUATION4].AddResult1(
			"Barista: But we'd be making a difference by complying! People need coffee to thrive! It helps scientists work their brain to make scientific advancements, teachers in the morning to educate kids, or volunteers to clean up wreckage sites. Wouldn't you agree?"
			);
		textBlockList[(int)GC.State.SITUATION4].AddResult1(
			"Bean: Well... No!"
			);
		// Restluts Text 2
		textBlockList[(int)GC.State.SITUATION4].AddResult2(
			"Bean response: You may have a point. But that doesn't mean I'm giving up!"
			);

		// DENEUMONT
		textBlockList[(int)GC.State.DENOUEMENT].AddNarrative(
			"Barista: ... You know, I've never hesitated making a cup of coffee before."
			);
		textBlockList[(int)GC.State.DENOUEMENT].AddNarrative(
			"bean: ..."
			);
		// Answer Text
		textBlockList[(int)GC.State.SITUATION4].AddAnswer1(
			"none"
			);
		textBlockList[(int)GC.State.SITUATION4].AddAnswer2(
			"none"
			);
		
		// ENDING 1
		textBlockList[(int)GC.State.ENDING1].AddNarrative(
			"CUSTOMER: I'll kill if I don't get my coffee right now! Worse yet, I'll sue!"
			);
		textBlockList[(int)GC.State.ENDING1].AddNarrative(
			"Barista: I'm so, so sorry about this! I'll give you a full refund too."
			);
		textBlockList[(int)GC.State.ENDING1].AddNarrative(
			"Manager: \"Sorry\" won't cut it. You've screwed up for the last time. Corporate will make sure of that."
			);
		textBlockList[(int)GC.State.ENDING1].AddNarrative(
			"You're FIRED! Game Over. Click to replay"
			);
		// Answer Text
		textBlockList[(int)GC.State.ENDING1].AddAnswer1(
			"none"
			);
		textBlockList[(int)GC.State.ENDING1].AddAnswer2(
			"none"
			);
		// Results Text 1
		textBlockList[(int)GC.State.ENDING1].AddResult1(
			"Test RESULT1"
			);
		// Restluts Text 2
		textBlockList[(int)GC.State.ENDING1].AddResult2(
			"Test RESULT2"
			);

		// ENDING2
		textBlockList[(int)GC.State.ENDING2].AddNarrative(
			"... but whatever."
			);
		textBlockList[(int)GC.State.ENDING2].AddNarrative(
			"Coffee Gods, accept my sacrifice!"
			);
		textBlockList[(int)GC.State.ENDING2].AddNarrative(
			"Bean: NOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO!!!!!"
			);
		textBlockList[(int)GC.State.ENDING2].AddNarrative(
			"BARISTA: Another day, another decaf. But I feel… dirty."
			);
		textBlockList[(int)GC.State.ENDING2].AddNarrative(
			"BARISTA: Enjoy, Mr. Wickhauser!"
			);
		textBlockList[(int)GC.State.ENDING2].AddNarrative(
			"CUSTOMER: Mm, mm, mm… Thank you so much! This is the perfect cup of coffee! Can you make me another?"
			);
		textBlockList[(int)GC.State.ENDING2].AddNarrative(
			"BARISTA: ..."
			);
		textBlockList[(int)GC.State.ENDING2].AddNarrative(
			"You have made the perfect cup of coffee. Congratulations? Game Over."
			);
		// Answer Text
		textBlockList[(int)GC.State.ENDING2].AddAnswer1(
			"none"
			);
		textBlockList[(int)GC.State.ENDING2].AddAnswer2(
			"none"
			);
		// Results Text 1
		textBlockList[(int)GC.State.ENDING2].AddResult1(
			"Test RESULT1"
			);
		// Restluts Text 2
		textBlockList[(int)GC.State.ENDING2].AddResult2(
			"Test RESULT2"
			);


		// ENDING3
		textBlockList[(int)GC.State.ENDING3].AddNarrative(
			"Barista: You know, I finally understand. I've always felt this way on the inside but have been too afraid to act on it. Let's make a better world, together."
			);
		textBlockList[(int)GC.State.ENDING3].AddNarrative(
			"You quit work and smuggle all the coffee beans out. With them, you drive off into the sunset, towards the headquarters of the coffee rebellion. Game Over"
			);
		// Answer Text
		textBlockList[(int)GC.State.ENDING3].AddAnswer1(
			"none"
			);
		textBlockList[(int)GC.State.ENDING3].AddAnswer2(
			"none"
			);
		// Results Text 1
		textBlockList[(int)GC.State.ENDING3].AddResult1(
			"Test RESULT1"
			);
		// Restluts Text 2
		textBlockList[(int)GC.State.ENDING3].AddResult2(
			"Test RESULT2"
			);


		// // TEMPLATE
		// textBlockList[(int)GC.State.TOTAL].AddNarrative(
		// 	"This is the first page of a test"
		// 	);
		// textBlockList[(int)GC.State.TOTAL].AddNarrative(
		// 	"This is the Second page of a test"
		// 	);
		// // Answer Text
		// textBlockList[(int)GC.State.TOTAL].AddAnswer1(
		// 	"none"
		// 	);
		// textBlockList[(int)GC.State.TOTAL].AddAnswer2(
		// 	"none"
		// 	);
		// // Results Text 1
		// textBlockList[(int)GC.State.TOTAL].AddResult1(
		// 	"Test RESULT1"
		// 	);
		// // Restluts Text 2
		// textBlockList[(int)GC.State.TOTAL].AddResult2(
		// 	"Test RESULT2"
		// 	);
	}

	// [[ ----- SEND STRING ----- ]]
}
