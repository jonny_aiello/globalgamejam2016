﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; 

public class StateMachine : MonoBehaviour {

// -----------------------------------------------------------------------------
// Variables
	
	private GC.State state;
	private GC.State lastState;
	private GC.State nextState; 
	private int lastDecision;
	// private int karmaScore;
	[HideInInspector] public TextHandler th;
	private InputHandler ih;
	// public GC.State startState; 
	public Printer p;
	[HideInInspector] public Persistant per;
	
	private Color startColor = new Color (1, 1, 1, 1); 
	private Color basementColor; 

	public GameObject moonbucksScene; 
	public GameObject innerScene; 
	public GameObject handScene; 

	// Use this for initialization
	void Start () {
		th = GetComponent<TextHandler>();
		ih = GetComponent<InputHandler>(); 
		// p = GetComponent<Printer>(); 
		if( !GameObject.Find("PersistantData").GetComponent<Persistant>() ){ 
			Debug.LogWarning("Can't Find Singleton!" + null);
		}
		else{ 
			per = GameObject.Find("PersistantData").GetComponent<Persistant>();
		}

		// set up game aesthetics
		basementColor = GameObject.Find("Main Camera").GetComponent<Camera>().backgroundColor; 

		// Starting GC.State
		state = per.openingState;
		lastState = GC.State.TOTAL;

		// load first textblock
		// p.LoadQueue( th.textBlockList[0] ); 

	}

// -----------------------------------------------------------------------------
// GC.State Machine Logic

	// [[ ----- UPDATE ----- ]]

	void Update () {
		// test
		// if( Input.GetButtonDown("Jump") ){ state = nextState; } 

		if( state != lastState ){ 
			if( state == GC.State.INTRO1 ){
				nextState = GC.State.INTRO2;
				GameObject.Find("Main Camera").GetComponent<Camera>().backgroundColor = startColor;  
				moonbucksScene.SetActive(true); 
				innerScene.SetActive(false);
				handScene.SetActive(false); 
				Debug.Log("STATE: " + state);
			}
			if( state == GC.State.INTRO2 ){
				nextState = GC.State.SITUATION1;
				GameObject.Find("Main Camera").GetComponent<Camera>().backgroundColor = startColor;  
				moonbucksScene.SetActive(false); 
				innerScene.SetActive(true);
				handScene.SetActive(false); 
				p.LoadQueue( th.textBlockList[(int)state], 0 );
				Debug.Log("STATE: " + state);
			}
			else if( state == GC.State.SITUATION1 ){
				nextState = GC.State.MINIGAME1;  
				GameObject.Find("Main Camera").GetComponent<Camera>().backgroundColor = basementColor; 
				moonbucksScene.SetActive(false); 
				innerScene.SetActive(false);
				handScene.SetActive(true); 
				p.LoadQueue( th.textBlockList[(int)state], 0 );
				// Debug.Log("STATE: " + state);
			} 
			else if( state == GC.State.MINIGAME1 ){
				nextState = GC.State.SITUATION2;  
				if( lastDecision == 2 ){ LoadMinigame(); } 
				// else{ LoadNextCh(); }
				// Debug.Log("STATE: " + state);
			} 
			else if( state == GC.State.SITUATION2 ){
				nextState = GC.State.MINIGAME2;
				moonbucksScene.SetActive(false); 
				innerScene.SetActive(false);
				handScene.SetActive(true);  
				p.LoadQueue( th.textBlockList[(int)state], 0 );
				Debug.Log("STATE: " + state);
			} 
			else if( state == GC.State.MINIGAME2 ){
				nextState = GC.State.SITUATION3; 
				if( lastDecision == 2 ){ LoadMinigame(); }
				Debug.Log("STATE: " + state);
			} 
			else if( state == GC.State.SITUATION3 ){
				nextState = GC.State.MINIGAME4;
				moonbucksScene.SetActive(false); 
				innerScene.SetActive(false);
				handScene.SetActive(true);  
				p.LoadQueue( th.textBlockList[(int)state], 0 );
				Debug.Log("STATE: " + state);
			} 
			else if( state == GC.State.MINIGAME3 ){
				nextState = GC.State.SITUATION4; 
				if( lastDecision == 2 ){ LoadMinigame(); }
				Debug.Log("STATE: " + state);
			} 
			else if( state == GC.State.SITUATION4 ){
				nextState = GC.State.MINIGAME4; 
				moonbucksScene.SetActive(false); 
				innerScene.SetActive(false);
				handScene.SetActive(true); 
				p.LoadQueue( th.textBlockList[(int)state], 0 );
				Debug.Log("STATE: " + state);
			} 
			else if( state == GC.State.MINIGAME4 ){
				nextState = GC.State.DENOUEMENT; 
				if( lastDecision == 2 ){ LoadMinigame(); }
				Debug.Log("STATE: " + state);
			} 
			else if( state == GC.State.DENOUEMENT ){
				Debug.Log("KarmaScore: " + per.karmaScore);
				if( per.karmaScore > 1 && per.karmaScore < 3 ){ nextState = GC.State.ENDING1; }
				else if( per.karmaScore < 3 ){ nextState = GC.State.ENDING2; }
				else if( per.karmaScore == 3 ){ nextState = GC.State.ENDING3; }
				moonbucksScene.SetActive(false); 
				innerScene.SetActive(false);
				handScene.SetActive(true); 
				p.LoadQueue( th.textBlockList[(int)state], 0 );
				Debug.Log("STATE: " + state);
			} 
			else if( state == GC.State.ENDING1 ){
				nextState = GC.State.THEEND; 
				GameObject.Find("Main Camera").GetComponent<Camera>().backgroundColor = startColor;  
				moonbucksScene.SetActive(false); 
				innerScene.SetActive(true);
				handScene.SetActive(false); 
				p.LoadQueue( th.textBlockList[(int)state], 0 );
				Debug.Log("STATE: " + state);
			} 
			else if( state == GC.State.ENDING2 ){
				nextState = GC.State.THEEND; 
				GameObject.Find("Main Camera").GetComponent<Camera>().backgroundColor = startColor;  
				moonbucksScene.SetActive(false); 
				innerScene.SetActive(true);
				handScene.SetActive(false); 
				p.LoadQueue( th.textBlockList[(int)state], 0 );
				Debug.Log("STATE: " + state);
			} 
			else if( state == GC.State.ENDING3 ){
				nextState = GC.State.THEEND; 
				GameObject.Find("Main Camera").GetComponent<Camera>().backgroundColor = startColor;  
				moonbucksScene.SetActive(false); 
				innerScene.SetActive(true);
				handScene.SetActive(false); 
				p.LoadQueue( th.textBlockList[(int)state], 0 );
				Debug.Log("STATE: " + state);
			} 
			else if( state == GC.State.THEEND ){
				nextState = GC.State.TOTAL; 
				Application.LoadLevel(2);
				Debug.Log("STATE: " + state);
			} 
		}

		lastState = state; 
	}


// -----------------------------------------------------------------------------
// External Methods
	
	// [[ ----- LOAD NEXT CH ----- ]]
	public void LoadNextCh(){
		state = nextState;
		Debug.Log("5 - state changed in sm" + null);
		// TEST
		 // p.LoadQueue( th.textBlockList[(int)state] ); 
	}

	// [[ ----- LOAD MINIGAME ----- ]]
	public void LoadMinigame(){
		per.openingState = nextState;
		Application.LoadLevel(1);
	}

	// [[ ----- SET DECISION ----- ]]
	public void SetDecision( int _n ){
		lastDecision = _n; 
		ih.narrativeMode = true; 
		p.DeactivateButtons(); 
		// pass result text
		p.LoadQueue(th.textBlockList[(int)state], _n);

		//add karma score
		if( _n == 1 ){ per.karmaScore++; Debug.Log("karmaScore: " + per.karmaScore); }
		Debug.Log("Decision updated: " + _n);
	}
}
