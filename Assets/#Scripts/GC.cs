﻿using UnityEngine;
using System.Collections;

public static class GC{

// -----------------------------------------------------------------------------
// Enums
	public enum State{ 
		INTRO1,
		INTRO2,
		SITUATION1,
		MINIGAME1,
		SITUATION2,
		MINIGAME2,
		SITUATION3,
		MINIGAME3,
		SITUATION4,
		MINIGAME4,
		DENOUEMENT,
		ENDING1,
		ENDING2,
		ENDING3,
		THEEND,
		TOTAL
	}

}
