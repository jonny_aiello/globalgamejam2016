﻿using UnityEngine;
using System.Collections;

public class Persistant : MonoBehaviour {
	
	public static Persistant singleton;
	
	public int karmaScore; 
	public GC.State openingState;
	
	void Awake () {
		// singleton logic
		if( singleton == null ){
			DontDestroyOnLoad( gameObject );
			singleton = this; 
		} else if( singleton != this ){
			Destroy( gameObject ); 
		}
	} 
	
	
}